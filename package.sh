#!/bin/sh
set -eu

if [ "x$1" = x ]; then
    echo "Version?" >&2
    exit 1
fi
WORKDIR="$(pwd)"

rm_except(){
    for i in *; do
        [ "$i" = "$1" ] || rm "$i"
    done
}

cd "$WORKDIR/linux"
tar zcf windfest-$1-linux-amd64.tar.gz *
rm_except windfest-$1-linux-amd64.tar.gz

cd "$WORKDIR/windows"
zip windfest-$1-windows.zip *
rm_except windfest-$1-windows.zip

cd "$WORKDIR/macos"
mv windfest.zip windfest-$1-macos.zip
