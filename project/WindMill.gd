extends Spatial

export(float) var speed = 1.0

func _physics_process(delta):
    $blades.rotate_x(speed * delta)
