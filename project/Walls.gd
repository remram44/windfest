extends Spatial

func _ready():
    var color = Color(rand_range(0.0, 1.0), rand_range(0.0, 1.0), rand_range(0.0, 1.0))
    $ParticlesL.draw_pass_1.surface_get_material(0).albedo_color = color
