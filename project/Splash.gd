extends Control

signal start

func _ready():
    $PrevScore.visible = false
    $mill.visible = false
    $mill_timer.start()

func show():
    visible = true
    $mill.visible = false
    $mill_timer.start()

func _process(delta):
    if visible and $mill.visible and Input.is_action_pressed("start"):
        visible = false
        emit_signal("start")

func set_score(score):
    $PrevScore.visible = true
    $PrevScore.text = "Your previous score was %d. Booh!" % score

func _on_mill_timer_timeout():
    $mill.visible = true
