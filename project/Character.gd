extends Spatial

signal collided

const MOVE_SPEED = 10.0

func _physics_process(delta):
    var move = 0.0
    move -= Input.get_action_strength("left")
    move += Input.get_action_strength("right")

    move = clamp(move, -1.0, 1.0)

    translate(Vector3(move * MOVE_SPEED * delta, 0, 0))

    translation.x = clamp(translation.x, -5.0, 5.0)

func _on_area_entered(area):
    emit_signal("collided")
