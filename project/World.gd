extends Spatial

var Chunk = preload("res://chunks/Chunk.tscn")
var Walls = preload("res://Walls.tscn")

onready var splash = $Splash

var chunks = []

var run_speed = 10.0
var run_speed_sq = 100.0
const RUN_SPEED_INC = 200.0

var score = 0

const NB_CHUNKS = 3

func setup():
    for chunk in chunks:
        chunk.queue_free()
    chunks = []
    run_speed = 10.0
    run_speed_sq = 100.0
    # Create initial chunks
    var pos = -15.0
    for i in range(NB_CHUNKS):
        var chunk = Chunk.instance()
        chunk.add_child(Walls.instance())
        add_child(chunk)
        chunk.translate(Vector3(0, 0, pos))
        pos -= 50.0
        chunks.push_back(chunk)

func _ready():
    randomize()

    setup()
    set_physics_process(false)

func _physics_process(delta):
    # Advance the chunks
    var translation = Vector3(0, 0, run_speed * delta)
    for i in range(NB_CHUNKS):
        chunks[i].translate(translation)

    score += run_speed * delta
    $Score.text = "Score: %d" % score

    # If first chunk is not visible anymore, drop it and get a new one
    if chunks[0].translation.z > 30.0:
        var old_chunk = chunks.pop_front()
        old_chunk.queue_free()
        var new_chunk = Chunk.instance()
        add_child(new_chunk)
        new_chunk.translate(Vector3(0, 0, chunks[-1].translation.z - 50.0))
        new_chunk.add_child(Walls.instance())
        chunks.push_back(new_chunk)
        run_speed_sq += RUN_SPEED_INC
        run_speed = sqrt(run_speed_sq)

func _on_start():
    score = 0
    setup()
    set_physics_process(true)

func _on_character_collided():
    if is_physics_processing():
        splash.show()
        set_physics_process(false)
        splash.set_score(score)
