extends Spatial

func _ready():
    var pos = randi() % 3 - 1
    $WindMill.translate(Vector3(pos * 4.0, 0, 0))
    var facing = randi() % 2
    if facing == 0:
        $WindMill.rotate_y(PI)
    var speed = rand_range(0.6, 4.0)
    $WindMill.speed = speed

    print("Created Chunk pos=%s facing=%s speed=%s" % [pos, facing, speed])
